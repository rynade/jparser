#-------------------------------------------------
#
# Project created by QtCreator 2013-06-15T23:04:40
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = Console
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    commandparser.cpp \
    parserexceptions.cpp \
    commandargument.cpp \
    commandsubargument.cpp \
    jserver.cpp

HEADERS += \
    commandparser.h \
    parserexceptions.h \
    commandargument.h \
    commandsubargument.h \
    jserver.h
