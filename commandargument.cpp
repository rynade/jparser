#include "commandargument.h"

namespace Parser
{
    void CommandArgument::AppendSubArg(CommandSubArgument* subArg)
    {
        SubArgs.push(subArg);
    }

    CommandArgument::CommandArgument(char arg[255])
    {
        Arg = arg;
    }

    CommandArgument::~CommandArgument()
    {
        delete(Arg);
        while(!SubArgs.empty())
        {
            delete(SubArgs.top());
            SubArgs.pop();
        }
    }
}
