#ifndef PARSERARGUMENTS_H
#define PARSERARGUMENTS_H
#include "commandsubargument.h"
#include "stack"

using namespace std;
namespace Parser
{
    class CommandArgument
    {
    public:
        CommandArgument(char arg[255]);
        void AppendSubArg(CommandSubArgument* subArg);
        char* Arg;
        stack<CommandSubArgument*> SubArgs;
        ~CommandArgument();
    };
}

#endif // PARSERARGUMENTS_H
