#include "commandparser.h"

namespace Parser
{
    CommandParser::CommandParser(char* fullCommand)
    {
        _fullCommand = fullCommand;
        ParseCommand();
    }

    int CommandParser::ParseArg(int* index)
    {
        int argCharIndex(0);

        char* argPlaceholder = (_fullCommand+*index);
        if(*argPlaceholder == '-')
        {
            ++*index;
        }
        else if(*argPlaceholder != '\0')
        {
            throw "Expected argument placeholder for argument";
        }

        char newArg[255];
        while((*(_fullCommand+*index) != ' ')&&(*(_fullCommand+*index) != '\0'))
        {
            newArg[argCharIndex] = *(_fullCommand+*index);
            argCharIndex++;
            ++*index;
        }
        CommandArgument* argument = new CommandArgument(newArg);
        Arguements.push(argument);

        RemoveWhitespace(index);
        char currentChar = *(_fullCommand+*index);
        if(currentChar == '-')
        {
            return 1;
        }
        else if(currentChar == '\0')
        {
            return -1;
        }
        else
        {
            ParseSubArgs(index, argument);
            currentChar = *(_fullCommand+*index);
            if(currentChar == '\0')
            {
                return -1;
            }
            return 1;
        }
    }

    void CommandParser::ParseSubArgs(int* index, CommandArgument* argument)
    {
        char subArg[255];
        int subArgIndex(0);
        for(;(*(_fullCommand+*index) != '\0')&&(*(_fullCommand+*index) != ' ');++*index)
        {
            subArg[subArgIndex] = *(_fullCommand+*index);
            subArgIndex++;
        }
        argument->AppendSubArg(new CommandSubArgument(subArg));
        RemoveWhitespace(index);
        char currentChar = *(_fullCommand+*index);
        if((currentChar == '-')||(currentChar == '\0'))
        {
            return;
        }
        else
        {
            ParseSubArgs(index, argument);
        }
    }

    void CommandParser::ParseArgs(int* index)
    {
        for(int argIndex(0); ; argIndex++)
        {
            RemoveWhitespace(index);
            int result = ParseArg(index);\
            if(result == -1)
            {
                break;
            }
        }
    }

    void CommandParser::ParseCommand()
    {
        int* i = new int(0);
        RemoveWhitespace(i);
        while(*(_fullCommand+*i) == '\0')
        {
            *i++;
        }
        for(; _fullCommand[*i] != ' ';++*i)
        {
            _command[*i] = *(_fullCommand+*i);
        }

        try
        {
            ParseArgs(i);
        }
        catch(...)
        {
            delete(i);
            throw;
        }
    }

    void CommandParser::RemoveWhitespace(int* index)
    {
        while(*(_fullCommand+*index) == ' ')
        {
            ++*index;
        }
    }

    CommandParser::~CommandParser()
    {
        delete(_fullCommand);
        while(!Arguements.empty())
        {
            delete(Arguements.top());
            Arguements.pop();
        }
    }
}


