#ifndef COMMANDPARSER_H
#define COMMANDPARSER_H
#include "commandargument.h"

using namespace std;
namespace Parser
{
    class CommandParser
    {
    public:
        CommandParser(char* fullCommand);
        stack<CommandArgument*> Arguements;
        ~CommandParser();
    private:
        char* _fullCommand;
        char _command[255];

        CommandParser();
        int ParseArg(int* index);
        void ParseArgs(int* index);
        void ParseSubArgs(int* index, CommandArgument* argument);
        void ParseCommand();
        void RemoveWhitespace(int* index);
    };
}

#endif // COMMANDPARSER_H
