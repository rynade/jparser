#ifndef COMMANDSUBARGUMENT_H
#define COMMANDSUBARGUMENT_H

namespace Parser
{
    class CommandSubArgument
    {
    public:
        CommandSubArgument(char arg[255]);
        char* SubArg;
        ~CommandSubArgument();
    };
}

#endif // COMMANDSUBARGUMENT_H
