#ifndef JSERVER_H
#define JSERVER_H

namespace Network
{
    class JServer
    {
    public:
        static void Start();

    private:
        JServer();
    };
}

#endif // JSERVER_H
